# SYSTEM REQUIRED
PHP >= 5.5.9

OpenSSL PHP Extension

PDO PHP Extension

Mbstring PHP Extension

Tokenizer PHP Extension


# EJABBERD API

Execute the following command to get the latest version of the package:

```terminal
	composer install
```

## Setup
Update configuration of database in `.env`
```php
APP_ENV=local
APP_DEBUG=true
APP_KEY=ZY1cZPDAeJnceWm7VcbRJn0lnmup5UTd



#EJABBERD SERVER
EJABBERD_ADDRESS=http://localhost:5285/rest

```

Only for MAC/LINUX
```shell
mkdir storage/framework/views
```
Edit swagger UI
```shell
/vendor/latrell/swagger/public/index.html

/vendor/latrell/swagger/src/views/index.blade.php

var apiKeyAuth = new SwaggerClient.ApiKeyAuthorization("AuthorizationUser", key, "header");

window.swaggerUi.api.clientAuthorizations.add("AuthorizationUser", apiKeyAuth);
            
```
==>change api_key to AuthorizationUser


Publish Configuration

```shell
chmod -R 777 storage/
rm -rf resources/views/vendor/latrell/
php artisan vendor:publish
```

Run migration and seeding

```shell
php artisan migrate
php artisan db:seed
```
## Swagger UI
Update configuration of Swagger UI  `config/latrell-swagger.php`
```shell
	'enable' => true,
	'default-base-path' => '{project server url link}/public'
```
Open link  {project server url link}/public/api-docs
