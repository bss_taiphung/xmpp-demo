<?php namespace App\Http\Middleware;
use Carbon\Carbon;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class SentinelAuthenticate
{

    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     *
     * @var ErrorCode
     */
    protected $errorCode;

    /**
     *
     * @var userToken
     */
    protected $userToken;


    /**
     * Create a new filter instance.
     *
     * @param  Request $request
     *
     * @return void
     */
    public function __construct(
        Request $request
    )
    {
        $this->request = $request;
        $this->apiErrorCode = Lang::get('apiErrorCode');
        $this->auth = null;
        $this->userToken = $this->request->header('AuthorizationUser');

        if ($this->userToken) {
            $this->auth = \Cartalyst\Sentinel\Laravel\Facades\Sentinel::getUserRepository()->findByPersistenceCode($this->userToken);
        }

    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth) {
            //logged
            $persistence = \Cartalyst\Sentinel\Laravel\Facades\Sentinel::getPersistenceRepository()->where('code',$this->userToken)->first();

            if($persistence->expired_at < Carbon::now()){ // Check token expired
                $response = array(
                    "error" => true,
                    "data" => null,
                    "errors" => array([
                        'errorCode'=>$this->apiErrorCode['ApiErrorCodes']['persistences_code_expired_at_token'],
                        'errorMessage'=>$this->apiErrorCode['persistences_code_expired_at_token']
                    ])
                );
                return response()->json($response, 500, array());
            }
            else
            {
                $request->attributes->add(['auth' => $this->auth]);
                return $next($request);
            }

        } else {
            //not logged yet
            $response = array(
                "error" => true,
                "data" => null,
                "errors" => array([
                    'errorCode'=>$this->apiErrorCode['ApiErrorCodes']['persistences_code_required_user_token'],
                    'errorMessage'=>$this->apiErrorCode['persistences_code_required_user_token']
                ])
            );
            return response()->json($response, 401, array());
        }
    }
}
