Install
=======
```bash
sudo npm install
bower install
```
Build stanza.io
===============

```bash
cd node_modules/stanza.io
sudo npm install && make
```

Build
=====
```bash
grunt watch --force
```