Start container
===============
```sh
docker run -d -t -h mongooseim-1 --name mongooseim-1 \
-p 4369:4369 \
-p 5222:5222 \
-p 5269:5269 \
-p 5280:5280 \
-p 9100:9100 \
mongooseim/mongooseim:latest
```

cd /usr/lib/mongooseim/bin
or
/usr/lib/mongooseim/bin/mongooseimctl

Register users
==============
```sh
./mongooseimctl register nasterblue localhost password &&\
./mongooseimctl register bss1 localhost password &&\
./mongooseimctl register bss2 localhost password &&\
./mongooseimctl register bss3 localhost password &&\
./mongooseimctl register bss4 localhost password &&\
./mongooseimctl register bss5 localhost password &&\
./mongooseimctl register bss6 localhost password &&\
./mongooseimctl register bss7 localhost password &&\
./mongooseimctl register bss8 localhost password &&\
./mongooseimctl register bss9 localhost password
```

Register rooms
==============
```sh
./mongooseimctl create_room nasterblue  muc.localhost localhost &&\
./mongooseimctl create_room nasterblue0  muc.localhost localhost &&\
./mongooseimctl create_room nasterblue1  muc.localhost localhost &&\
./mongooseimctl create_room nasterblue2  muc.localhost localhost &&\
./mongooseimctl create_room nasterblue3  muc.localhost localhost &&\
./mongooseimctl create_room nasterblue4  muc.localhost localhost &&\
./mongooseimctl create_room nasterblue5  muc.localhost localhost &&\
./mongooseimctl create_room nasterblue6  muc.localhost localhost &&\
./mongooseimctl create_room nasterblue7  muc.localhost localhost &&\
./mongooseimctl create_room nasterblue8  muc.localhost localhost &&\
./mongooseimctl create_room nasterblue9  muc.localhost localhost
```

Send invites
============
```sh
./mongooseimctl send_direct_invitation nasterblue muc.localhost  none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost  &&\
./mongooseimctl send_direct_invitation nasterblue0 muc.localhost none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost &&\
./mongooseimctl send_direct_invitation nasterblue1 muc.localhost none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost &&\
./mongooseimctl send_direct_invitation nasterblue2 muc.localhost none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost &&\
./mongooseimctl send_direct_invitation nasterblue3 muc.localhost none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost &&\
./mongooseimctl send_direct_invitation nasterblue4 muc.localhost none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost &&\
./mongooseimctl send_direct_invitation nasterblue5 muc.localhost none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost &&\
./mongooseimctl send_direct_invitation nasterblue6 muc.localhost none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost &&\
./mongooseimctl send_direct_invitation nasterblue7 muc.localhost none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost &&\
./mongooseimctl send_direct_invitation nasterblue8 muc.localhost none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost &&\
./mongooseimctl send_direct_invitation nasterblue9 muc.localhost none none nasterblue@localhost:bss1@localhost:bss2@localhost:bss3@localhost:bss4@localhost:bss5@localhost:bss6@localhost:bss7@localhost:bss8@localhost:bss9@localhost

```