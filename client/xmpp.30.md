```bash
nasterblue.ejabberd.com


ejabberdctl create_room nasterblue  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com &&\
ejabberdctl create_room nasterblue0  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com &&\
ejabberdctl create_room nasterblue1  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com &&\
ejabberdctl create_room nasterblue2  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com &&\
ejabberdctl create_room nasterblue3  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com &&\
ejabberdctl create_room nasterblue4  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com &&\
ejabberdctl create_room nasterblue5  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com &&\
ejabberdctl create_room nasterblue6  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com &&\
ejabberdctl create_room nasterblue7  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com &&\
ejabberdctl create_room nasterblue8  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com &&\
ejabberdctl create_room nasterblue9  conference.nasterblue.ejabberd.com nasterblue.ejabberd.com


ejabberdctl muc_online_rooms nasterblue.ejabberd.com

ejabberdctl  destroy_room nasterblue conference.nasterblue.ejabberd.com

ejabberdctl get_room_affiliations nasterblue conference.nasterblue.ejabberd.com
ejabberdctl get_room_occupants nasterblue@conference.nasterblue.ejabberd.com nasterblue.ejabberd.com
ejabberdctl get_room_occupants_number nasterblue conference.nasterblue.ejabberd.com
ejabberdctl srg_get_members nasterblue@conference.nasterblue.ejabberd.com nasterblue.ejabberd.com
ejabberdctl get_user_rooms nasterblue nasterblue.ejabberd.com


ejabberdctl send_direct_invitation nasterblue conference.nasterblue.ejabberd.com  none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com  &&\
ejabberdctl send_direct_invitation nasterblue0 conference.nasterblue.ejabberd.com none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com &&\
ejabberdctl send_direct_invitation nasterblue1 conference.nasterblue.ejabberd.com none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com &&\
ejabberdctl send_direct_invitation nasterblue2 conference.nasterblue.ejabberd.com none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com &&\
ejabberdctl send_direct_invitation nasterblue3 conference.nasterblue.ejabberd.com none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com &&\
ejabberdctl send_direct_invitation nasterblue4 conference.nasterblue.ejabberd.com none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com &&\
ejabberdctl send_direct_invitation nasterblue5 conference.nasterblue.ejabberd.com none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com &&\
ejabberdctl send_direct_invitation nasterblue6 conference.nasterblue.ejabberd.com none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com &&\
ejabberdctl send_direct_invitation nasterblue7 conference.nasterblue.ejabberd.com none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com &&\
ejabberdctl send_direct_invitation nasterblue8 conference.nasterblue.ejabberd.com none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com &&\
ejabberdctl send_direct_invitation nasterblue9 conference.nasterblue.ejabberd.com none none nasterblue@nasterblue.ejabberd.com:bss1@nasterblue.ejabberd.com:bss2@nasterblue.ejabberd.com:bss3@nasterblue.ejabberd.com:bss4@nasterblue.ejabberd.com:bss5@nasterblue.ejabberd.com:bss6@nasterblue.ejabberd.com:bss7@nasterblue.ejabberd.com:bss8@nasterblue.ejabberd.com:bss9@nasterblue.ejabberd.com



ejabberdctl set_room_affiliation nasterblue conference.nasterblue.ejabberd.com nasterblue@192.168.99.100 member



$  ejabberdctl get_presence bss1 192.168.99.100
bss1@192.168.99.100/113592750254837369611492567578321615	available
$ ejabberdctl muc_online_rooms nasterblue.ejabberd.com
nasterblue9@conference.192.168.99.100
nasterblue8@conference.192.168.99.100
nasterblue7@conference.192.168.99.100
nasterblue6@conference.192.168.99.100
nasterblue5@conference.192.168.99.100
nasterblue4@conference.192.168.99.100
nasterblue3@conference.192.168.99.100
nasterblue2@conference.192.168.99.100
nasterblue1@conference.192.168.99.100
nasterblue0@conference.192.168.99.100
nasterblue@conference.192.168.99.100


##Change an affiliation in a MUC room. Affiliation can be one of: owner, admin, member, outcast, none.

ejabberdctl set_room_affiliation nasterblue conference.nasterblue.ejabberd.com bss1@192.168.99.100 member &&\
ejabberdctl set_room_affiliation nasterblue0 conference.nasterblue.ejabberd.com bss1@192.168.99.100 member &&\
ejabberdctl set_room_affiliation nasterblue1 conference.nasterblue.ejabberd.com bss1@192.168.99.100 member &&\
ejabberdctl set_room_affiliation nasterblue2 conference.nasterblue.ejabberd.com bss1@192.168.99.100 member &&\
ejabberdctl set_room_affiliation nasterblue3 conference.nasterblue.ejabberd.com bss1@192.168.99.100 member &&\
ejabberdctl set_room_affiliation nasterblue4 conference.nasterblue.ejabberd.com bss1@192.168.99.100 member &&\
ejabberdctl set_room_affiliation nasterblue5 conference.nasterblue.ejabberd.com bss1@192.168.99.100 member &&\
ejabberdctl set_room_affiliation nasterblue6 conference.nasterblue.ejabberd.com bss1@192.168.99.100 member &&\
ejabberdctl set_room_affiliation nasterblue7 conference.nasterblue.ejabberd.com bss1@192.168.99.100 member &&\
ejabberdctl set_room_affiliation nasterblue8 conference.nasterblue.ejabberd.com bss1@192.168.99.100 member &&\
ejabberdctl set_room_affiliation nasterblue9 conference.nasterblue.ejabberd.com bss1@192.168.99.100 member

##Get the list of affiliations of a MUC room.

ejabberdctl get_room_affiliations nasterblue conference.192.168.99.100
ejabberdctl get_room_affiliations nasterblue0 conference.192.168.99.100

```

```bash
vim conf/ejabberd.yml
ejabberdctl restart
```

```bash
   mod_muc:
     host: "conference.@HOST@"
     access: muc
     access_create: muc_create
     access_persistent: muc_create
     access_admin: muc_admin
     history_size: 50
     default_room_options:
       mam: true
       public: true
       public_list: true
       allow_change_subj: true
       allow_user_invites: true
       members_only: false
       members_by_default: true
       anonymous: true
       allow_private_messages: true
       persistent: true
   mod_muc_admin: {}
 
   mod_muc_log: {}
   
```

```bash

```