
/**
 * Created by nasterblue on 5/30/16.
 */
angular.module('directives')

    .directive('imroom', function ($timeout, $log, $http, $templateCache, $compile) {
        return {
            restrict: 'E',
            scope: {
                localUser: '=',
                chat: '=',
                embedOptions : '='
            },
            templateUrl: 'directives/imroom/imroom.tpl.html'
        };
    })

;
