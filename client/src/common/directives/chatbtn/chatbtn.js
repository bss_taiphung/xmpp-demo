/**
 * Created by nasterblue on 5/30/16.
 */
angular.module('directives')

    .directive('chatbtn', function ($timeout, $log) {
        return {
            restrict: 'AE',
            templateUrl: 'directives/chatbtn/chatbtn.tpl.html',
            scope :{
                xmppClient : '=',
                message : '=',
                sendMessage : '&'
            },
            link: function (scope, $element, $attrs) {

                scope.onTyping = function(chatState){
                    // if(scope.message.type!=="groupchat"){
                    //     var msg = {
                    //         type: 'chat',
                    //         to: scope.message.to,
                    //         from: scope.message.from,
                    //         chatState: chatState
                    //     };
                    //     scope.xmppClient.sendMessage(msg);
                    // }

                    var msg = {
                        type: 'chat',
                        to: scope.message.to,
                        from: scope.message.from,
                        chatState: chatState
                    };
                    scope.xmppClient.sendMessage(msg);
                };

                scope.typing = false;
                $element.on('keyup', function () {
                    if (scope.typing) {
                        if (scope.pendingPromise) {
                            $timeout.cancel(scope.pendingPromise);
                        }
                        scope.pendingPromise = $timeout(function () {
                            scope.typing = false;
                            scope.$apply(function(){
                                scope.onTyping('paused');
                            });
                        }, 2000);
                    }
                });

                $element.on('keydown', function (e) {
                    if (!scope.typing) {
                        scope.typing = true;
                        scope.onTyping('composing');
                    }
                });
            }
        };
    })

;
