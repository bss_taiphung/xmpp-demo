Ejabberd + MariaDB + phpMyadmin
================================

```bash
docker-compose up -d

mod_muc: 
    default_room_options:
        mam: true
        public: false
        public_list: false
        allow_change_subj: false
        allow_user_invites: false
        members_only: false
        members_by_default: false
        anonymous: false
        allow_private_messages: true
        persistent: true
        
     mod_carboncopy: 
        db_type: mnesia
```